#include <stdlib.h>
#include <stdio.h>

#include "common.h"
#include "parser.h"



int main() {
    int ret = 0;
    char *str = "42";
    parser_node_t *node = parser_parse(str);

    if (node->body->value.intVal != atoi(str)) {
        ret = 1;
    }

    parser_free_node(node);

    return ret;
}