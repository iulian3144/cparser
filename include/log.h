#ifndef _LOG_H_
#define _LOG_H_

void log_info(const char *, ...);
void log_debug(const char *, ...);
void log_fatal(const char *, ...);

#endif /* _LOG_H_ */
