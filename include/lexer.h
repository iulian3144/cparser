#ifndef _LEXER_H_
#define _LEXER_H_

#include <stdio.h>

#include "common.h"

typedef struct {
    const char *path;
    size_t lineno;
    size_t colno;
} file_location_t;

typedef enum {
    T_AND,
    T_BREAK,
    T_CONTINUE,
    T_ELIF,
    T_ELSE,
    T_ENDFOREACH,
    T_ENDIF,
    T_FALSE,
    T_FOREACH,
    T_IF,
    T_IN,
    T_NOT,
    T_OR,
    T_TRUE,
    T_LAST_KEYWORD = T_TRUE,

    // Operators
    T_ASSIGN,
    T_COLON,
    T_COMMA,
    T_DOT,
    T_EQ,
    T_GEQ,
    T_GT,
    T_LBRACK,
    T_LCURL,
    T_LEQ,
    T_LPAREN,
    T_LT,
    T_MINEQ,
    T_MINUS,
    T_MODEQ,
    T_MODULO,
    T_NEQ,
    T_PLUS,
    T_PLUSEQ,
    T_QUESTION,
    T_RBRACK,
    T_RCURL,
    T_RPAREN,
    T_SLASH,
    T_SLASHEQ,
    T_STAR,
    T_STAREQ,
    T_LAST_OP = T_STAREQ,

    // Data
    T_IDENTIFIER,
    T_NUMBER,
    T_STRING,

    // Magic
    T_EOF,
    T_EOL,
    T_ERROR,
} TOKEN_TYPE;

typedef struct {
    file_location_t loc;
    TOKEN_TYPE type;
    struct {
        size_t n;
        char *data;
    } string;
} lex_token_t;

typedef struct {
    FILE *file;
    file_location_t loc;
    lex_token_t prev;
    int cur;
} lexer_t;



/**
 * @brief Initialize lexer
 * 
 * @param lexer lexer object to initialize
 * @param path path of input file
 */
void lexer_init(lexer_t *lexer, const char *path);

/**
 * @brief Get next token
 *
 * @param[in] lexer lexer object to perform analysis on
 * @param[out] token output token
 * @return token type
 */
int lexer_lex(lexer_t *lexer, lex_token_t *token);

/**
 * @brief Revert lexer_lex action
 *
 * @param lexer lexer object to perform analysis on
 * @return token type
 */
int lexer_unlex(lexer_t *lexer);

/**
 * @brief Print the error message buffer
 * 
 * @param lexer lexer object
 * @param token token for which to print the error
 */
void lexer_print_error(lexer_t *lexer, lex_token_t *token);

#endif /* _LEXER_H_ */
