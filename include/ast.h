#ifndef _AST_H_
#define _AST_H_

#include <stddef.h>

#include "common.h"

typedef enum {
	AST_NONE,
	AST_ASSIGNMENT,
	AST_CONDITION,
	AST_OR,
	AST_AND,
	AST_EQUALITY,
	AST_RELATION,
	AST_ADDITION,
	AST_MULTIPLICATION,
	AST_UNARY,
	AST_IDENTIFIER,
	AST_STRING,
	AST_ARRAY,
	AST_NUMBER,
} AST_TYPE;

typedef enum {
    AST_NUMBASE_BIN = 2,
    AST_NUMBASE_OCT = 8,
    AST_NUMBASE_DEC = 10,
    AST_NUMBASE_HEX = 16
} AST_NUMBER_BASE;

typedef struct {
    i64 value;
    AST_NUMBER_BASE base;
} ast_number_t;

typedef struct {
    char *data;
    size_t n;
} ast_identifier_t;

typedef struct {
    char *data;
    size_t n;
} ast_string_t;

#endif /* _AST_H_ */
