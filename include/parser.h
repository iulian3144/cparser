#ifndef _PARSER_H_
#define _PARSER_H_

#include "common.h"



typedef struct parser_node parser_node_t;
typedef struct {
    char *str;
    parser_node_t *body;
} parser_ctx_t;

typedef enum {
    VAL_TYPE_INVALID,
    VAL_TYPE_PROGRAM,
    VAL_TYPE_CHAR,
    VAL_TYPE_SHORT,
    VAL_TYPE_FLOAT,
    VAL_TYPE_INT,
    VAL_TYPE_LONG,
    VAL_TYPE_POINTER,
    VAL_TYPE_DOUBLE,
} INTERP_VAL_TYPE;

typedef union {
    void   *pointer;
    char   charVal;
    i16    shortVal;
    i32    intVal;
    i64    longVal;
    float  floatVal;
    double doubleVal;
} interp_type_val_t;

typedef struct parser_node {
    INTERP_VAL_TYPE type;
    interp_type_val_t value;
    parser_node_t *body;
} parser_node_t;

parser_node_t *parser_parse(char *str);

/**
 * @brief Parse a program
 * Main entry point.
 *
 * Program
 *     : FunctionCall
 *     ;
 * @return i32 0 if success; !0 otherwise
 */
parser_node_t *parser_program(parser_ctx_t *ctx);

/**
 * @brief Parse a numeric literal
 *
 * NumericLiteral
 *     : NUMBER
 *     ;
 * @param ctx parser context
 * @return parser_ctx_t* 
 */
parser_node_t *parser_parse_number(parser_ctx_t *ctx);

/**
 * @brief Parse a function call
 *
 * FunctionCall
 *     : IDENTIFIER "(" ")"  // call with no arguments
 *     | IDENTIFIER "(" ARGUMENT_LIST ")"  // call with one or more arguments
 *     ;
 * @param ctx parser context
 * @return parser_ctx_t* 
 */
parser_node_t *parser_parse_fcall(parser_ctx_t *ctx);

parser_node_t *parser_new_node(void);
void parser_free_node(parser_node_t *node);

void parser_print_node(parser_node_t *node, u32 depth);

#endif /* _PARSER_H_ */
