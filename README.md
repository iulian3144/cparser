# What
A simple parser for parsing a limited set of C-like expressions.

# Why
Learn how to implement a manual recursive descent parser.

# Features
* [TODO] Parse integers (bin, oct, dec, hex)
* [TODO] Parse identifiers
* [TODO] Parse function calls
* [TODO] Parse symbol definitions (.e.g `my_ident = 10`)

# Compiling
```
cparser$ meson setup build
cparser$ ninja -C build
# run tests
cparser$ meson test -C build
```
