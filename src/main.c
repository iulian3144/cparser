#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "common.h"
#include "parser.h"
#include "lexer.h"



int main(int argc, char *argv[]) {
    printf("Recursive descent parser\n");

    lexer_t lexer;
    lex_token_t token;
    lexer_init(&lexer, NULL);
    TOKEN_TYPE tt = lexer_lex(&lexer, &token);
    if (tt == T_ERROR) {
        lexer_print_error(&lexer, &token);
        return 1;
    }

    printf("token type: %d\n", tt);
    return 0;
}
