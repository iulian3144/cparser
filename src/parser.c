/**
 * @file Parser.c
 * @brief Main methods for parsing a simple grammar
 */

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>

#include "common.h"
#include "interpreter.h"
#include "lexer.h"
#include "log.h"
#include "parser.h"



parser_node_t *parser_parse(char *str) {
    parser_ctx_t ctx;

    ctx.str = malloc(INTERP_COMMAND_LENGTH_MAX);
    strncpy(ctx.str, str, INTERP_COMMAND_LENGTH_MAX);

    ctx.body = parser_program(&ctx);

    return ctx.body;
}



parser_node_t *parser_program(parser_ctx_t *ctx) {
    parser_node_t *node = parser_new_node();
    node->type = VAL_TYPE_PROGRAM;
    node->body = parser_parse_number(ctx);
    return node;
}



parser_node_t *parser_parse_number(parser_ctx_t *ctx) {
    parser_node_t *node = parser_new_node();
    node->type = VAL_TYPE_INT;
    node->value.intVal = atoi(ctx->str);
    return node;
}



parser_node_t *parser_parse_fcall(parser_ctx_t *ctx) {
    // TODO: are there any arguments?
    return NULL;
}

parser_node_t *parser_new_node(void) {
    parser_node_t *node = malloc(sizeof(parser_node_t));
    log_debug("new %p", node);
    node->body = NULL;
    node->type = VAL_TYPE_INVALID;

    return node;
}

void parser_free_node(parser_node_t *node) {
    if (node == NULL) {
        return;
    }

    /* descend */
    parser_free_node(node->body);
    log_debug("free %p", node);
    free(node);
}



static char *char2str_map[] = {
    "VAL_TYPE_INVALID",
    "VAL_TYPE_PROGRAM",
    "VAL_TYPE_CHAR",
    "VAL_TYPE_SHORT",
    "VAL_TYPE_FLOAT",
    "VAL_TYPE_INT",
    "VAL_TYPE_LONG",
    "VAL_TYPE_POINTER",
    "VAL_TYPE_DOUBLE"
};

void parser_print_node(parser_node_t *node, u32 depth) {
    if (node == NULL) {
        return;
    }

    char indent[101] = "";
    char inner_indent[101] = "  ";


    for (int i = 0; i < depth; i++) {
        strncpy(indent, inner_indent, 100);
        strncat(inner_indent, indent, 100);
    }

    printf("%s{\n", indent);
    printf("%stype: %s\n", inner_indent, char2str_map[node->type]);
    if (node->type == VAL_TYPE_INVALID) {
        printf("%s}\n", indent);
        return;
    }
    if (node->type == VAL_TYPE_PROGRAM) {
        parser_print_node(node->body, ++depth);
    } else {
        switch (node->type) {
        case VAL_TYPE_POINTER:
            printf("%svalue: %p\n", inner_indent, node->value.pointer);
            break;
        case VAL_TYPE_SHORT:
            printf("%svalue: %d\n", inner_indent, node->value.shortVal);
            break;
        case VAL_TYPE_INT:
            printf("%svalue: %d\n", inner_indent, node->value.intVal);
            break;
        case VAL_TYPE_LONG:
            printf("%svalue: %ld\n", inner_indent, node->value.longVal);
            break;
        case VAL_TYPE_CHAR:
            printf("%svalue: `%c`\n", inner_indent, node->value.charVal);
            break;
        case VAL_TYPE_FLOAT:
            printf("%svalue: `%f`\n", inner_indent, node->value.floatVal);
            break;
        case VAL_TYPE_DOUBLE:
            printf("%svalue: `%f`\n", inner_indent, node->value.doubleVal);
            break;
        default:
            break;
        }
    }
    printf("%s}\n", indent);
}
