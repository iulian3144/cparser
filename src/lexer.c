#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "common.h"
#include "log.h"
#include "lexer.h"



static int lexer_next(lexer_t *lexer, file_location_t *loc);
static int lexer_push_back(lexer_t *lexer);
static TOKEN_TYPE lexer_number(lexer_t *lexer, lex_token_t *token);
static TOKEN_TYPE lexer_identifier(lexer_t *lexer, lex_token_t *token);



void lexer_init(lexer_t *lexer, const char *path) {
    memset(lexer, 0, sizeof(lexer_t));

    if (path != NULL) {
        lexer->file = fopen(path, "r");
        lexer->loc.path = path;
    } else {
        /* use STDIN if path is NULL */
        lexer->file = stdin;
        lexer->loc.path = "<stdin>";
    }

    if (lexer->file == NULL) {
        log_fatal("Failed to open file '%s'", path);
    }

    lexer->prev.type = T_ERROR;
    lexer->loc.lineno = 1;
    lexer->loc.colno = 0;
}



int lexer_lex(lexer_t *lexer, lex_token_t *token) {
    if (lexer->prev.type != T_ERROR) {
        *token = lexer->prev;
        lexer->prev.type = T_ERROR;
        return token->type;
    }

    int c;
    /* skip spaces */
    while ((c = lexer_next(lexer, &token->loc)) && isspace(c));

    if (isalpha(c) || c == '_'){
        return lexer_identifier(lexer, token);
    } else if (isdigit(c)) {
        return lexer_number(lexer, token);
    }

    return c;
}



int lexer_unlex(lexer_t *lexer) {
    return 0;
}



void lexer_print_error(lexer_t *lexer, lex_token_t *token) {
    char loc_str[1024] = {0};
    memset(loc_str, ' ', token->string.n);
    sprintf(loc_str+token->string.n, "^");
    printf("%s\n\033[1;37m%s:%zu:%zu\033[0m \033[1;31merror:\033[0m %s\n", loc_str,
        lexer->loc.path, lexer->loc.lineno, lexer->loc.colno-1, token->string.data);
}



/**
 * @brief Read next character
 * 
 * @param[in] lexer lexer object
 * @param[out] loc location in the file
 * @return int 
 */
static int lexer_next(lexer_t *lexer, file_location_t *loc) {
    int c = getc(lexer->file);
    if (c == '\n') {
        lexer->loc.lineno += 1;
        lexer->loc.colno = 1;
    } else if (c == '\t') {
        lexer->loc.colno += 8;
    } else {
        lexer->loc.colno++;
    }

    if (loc) {
        loc->path = lexer->loc.path;
        loc->lineno = lexer->loc.lineno;
        loc->colno = lexer->loc.colno;
    }

    lexer->cur = c;
    return c;
}



/**
 * @brief Get previous char
 * 
 * @param lexer lexer object
 * @return int location in the file
 */
static int lexer_push_back(lexer_t *lexer)
{
    int c = ungetc(lexer->cur, lexer->file);

    if (c == EOF) {
        log_fatal("failed to ungetc");
    } else if (c == '\n') {
        lexer->loc.lineno -= 1;
    }

    lexer->cur = 0;
    return c;
}



static u8 isnumber(int c)
{
    /* "xX" is valid hex, "oO" is valid octal, "bB" is valid binary */
    return (isxdigit(c) || c == 'x' || c == 'X'
            || c == 'o' || c == 'O' || c == 'b' || c == 'B');
}



/**
 * @brief Tokenize a number
 * 
 * @param lexer lexer object
 * @param token output token
 * @return token type
 */
static TOKEN_TYPE lexer_number(lexer_t *lexer, lex_token_t *token) {
    size_t n = 1;
    char buf[256] = {0};
    buf[0] = lexer->cur;

    int c;
    while ((c = lexer_next(lexer, NULL)) && isnumber(c)) {
        buf[n++] = c;
    }

    token->type = T_NUMBER;

    if (!isspace(c)) {
        snprintf(buf, sizeof(buf), "Unexpected character: '%c' is not a valid number", c);
        token->type = T_ERROR;
    }

    token->string.data = strdup(buf);
    token->string.n = n;

    return token->type;
}



/**
 * @brief Tokenize an identifer
 * 
 * @param lexer lexer object
 * @param token output token
 * @return token type
 */
static TOKEN_TYPE lexer_identifier(lexer_t *lexer, lex_token_t *token) {
    size_t n = 1;
    char buf[256] = {0};
    buf[0] = lexer->cur;

    int c;
    while ((c = lexer_next(lexer, NULL)) && isnumber(c)) {
        buf[n++] = c;
    }

    token->type = T_IDENTIFIER;
    return token->type;
}


